#include <stdio.h>

void pull(int(*ch)[100], int i, int j, int target,int*max)
{
	if (ch[i][j] + target <= 100)
	{
		*(*(ch + i) + j) += target;
	}
	else
	{
		*(*(ch + i) + j) = 100;
		pull(ch, i + 1, j, (target - 100) / 2,max);
		pull(ch, i + 1, j + 1, (target - 100) / 2,max);
		*max = (j > *max) ? j : *max;
	}
	*max = (j > *max) ? j : *max;

}
int main()
{
	int a, max=0;
	scanf_s("%d", &a);
	int ch[100][100] = { 0 };
	pull(ch, 0, 0, a,&max);
	int n = 0;
	for (; n <= max; n++)
	{
		int m;
		for( m=max-n;m>0;m--)
		{
			printf("  ");
		}
		int k = 0;
		for (; k < n + 1; k++)
		{
			if (ch[n][k] != 0)
				printf("%3d ", ch[n][k]);
		}
		printf("\n");
	}
	return 0;
}

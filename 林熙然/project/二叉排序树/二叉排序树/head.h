#pragma once
#include<stdio.h>
#include<stdlib.h>
typedef struct node
{
	int date;
	node* lchild, * rchild;
}tree;
void init(tree** t);
void initinsert(tree** t, int n);
void search(tree* t, int n);
void insert(tree** t, int n);
void shanchutwo(tree** t);
void shanchu(tree** t, int n);
void preorder(tree* t);
void inorder(tree* t);
void postorder(tree* t);
void rudui(tree* t);
void chudui(tree* t);
void cxorder(tree* t);
void menu();
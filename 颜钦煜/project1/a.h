


#ifndef __A_H__
#define __A_H__


#include <stdio.h>
#include <stdlib.h>

typedef struct node{

    int data=0;
	struct node *next=NULL;
}node,*linklist;//定义链表结构

void int_link(linklist front,linklist rear);//初始化队列
void add_link(linklist front,linklist rear);//尾插
void print_link(linklist front,linklist rear);//打印队列
void delete_link(linklist front,linklist rear);//头删
void find_link(linklist front,linklist rear,int t);//按值查找
void whole_delete_link(linklist front,linklist rear);//销毁队列
void menu();//菜单

#endif





#include <iostream>

typedef struct node{

    int data;
	struct node *lchild;
	struct node *rchild;
}node,*Bitree;//定义二叉树结构

typedef struct pnode{
	Bitree tree_data;
	struct pnode *next;
	
}pnode,*queue;//定义队列结构

int create_Bitree(Bitree &T,int number);//创建二叉树
void front_traversal(Bitree T);//前序遍历
void mid_traversal(Bitree T);//中序遍历
void rear_traversal(Bitree T);//后序遍历
void find_Bitree(Bitree T, int number);//按值查找
void delete_Bitree(Bitree T,Bitree s);//删除结点
void delete_find_Bitree(Bitree T, int number);//删除过程中的查找
void push(queue front,queue rear,Bitree T);	//入队
void pop(queue front,queue rear);//出队
void sequence_traversal(Bitree T,queue front,queue rear);//层序遍历
void menu();//菜单


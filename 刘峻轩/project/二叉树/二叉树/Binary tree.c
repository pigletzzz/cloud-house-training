//1. 初始化：输入一串数字，以第一个结点为根结点，从前往后插入结点进行初始化。//完成
//2. 查找//完成
//3. 插入//完成
//4. 删除//完成
//5. 三种遍历//完成
//6. 层序遍历

#include "Binary tree.h"
//树节点结构
typedef struct BitNode
{
	int data;
	struct BitNode* lchild, * rchild;
}BitNode;

// 用于层序遍历的队列结点结构
typedef struct QueueNode 
{
	BitNode* data;
	struct QueueNode* next;
} QueueNode;

// 用于层序遍历的队列结构
typedef struct Queue 
{
	QueueNode* front;
	QueueNode* rear;
} Queue;

// 初始化用于层序遍历的队列
void initQueue(Queue* queue) 
{
	queue->front = NULL;
	queue->rear = NULL;
}
// 入队
void enqueue(Queue* queue, BitNode* data) 
{
	QueueNode* newNode = (QueueNode*)malloc(sizeof(QueueNode));
	newNode->data = data;
	newNode->next = NULL;
	if (queue->front == NULL) 
	{
		queue->front = newNode;
		queue->rear = newNode;
	}
	else 
	{
		queue->rear->next = newNode;
		queue->rear = newNode;
	}
}
// 出队
BitNode* dequeue(Queue* queue) 
{
	if (queue->front == NULL) 
	{
		return NULL;
	}
	QueueNode* temp = queue->front;
	BitNode* data = temp->data;
	queue->front = queue->front->next;
	if (queue->front == NULL) 
	{
		queue->rear = NULL;
	}
	free(temp);
	return data;
}
// 销毁队列
void destroyQueue(Queue* queue) 
{
	while (queue->front != NULL) 
	{
		QueueNode* temp = queue->front;
		queue->front = queue->front->next;
		free(temp->data); // 释放队列节点中数据（这里假设队列节点中存储的是二叉树节点）
		free(temp);       // 释放队列节点
	}
	// 最后释放队列本身
	free(queue);
}
//菜单
void menu()
{
	printf("********************************\n");
	printf("***           菜单           ***\n");
	printf("***1.插入              2.删除***\n");
	printf("***3.查找              4.三遍***\n");
	printf("***5.层遍              0.退出***\n");
	printf("********************************\n");
}

BitNode* Search(BitNode* T,int Data,int *i, BitNode* p ,BitNode* f )//查找?? 书上f问题
{
	*i = 0;
	if (!T)
	{
		p = f;
		*i = 1;
		return p;
	}
	else if (Data == T->data)
	{
		p = T;
		return p;
	}
	else if (Data < T->data)
	{
		f = T;
		return Search(T->lchild, Data, i, p, f);
	}
	else if (Data > T->data)
	{
		f = T;
		return Search(T->rchild, Data, i, p, f);
	}
}
//查找
BitNode* DeleteSearch(BitNode* T, int Data, int* i, BitNode* p, BitNode** f)
{
	*i = 0;
	if (!T)
	{
		p = *f;
		*i = 1;
		return p;
	}
	else if (Data == T->data)
	{
		p = T;
		return p;
	}
	else if (Data < T->data)
	{
		*f = T;
		return DeleteSearch(T->lchild, Data, i, p, f);
	}
	else if (Data > T->data)
	{
		*f = T;
		return DeleteSearch(T->rchild, Data, i, p, f);
	}
}

//添加
void AddNode(BitNode* treeroot,int Data,int* b, BitNode* p1, BitNode* f)
{
	int i = 0;
	BitNode* p= treeroot;
	BitNode* pre= Search(treeroot, Data, &i,p1,f);
	*b = i;
	if (i)
	{
		BitNode* new = (BitNode*)malloc(sizeof(BitNode));
		new->data = Data;
		new->lchild = NULL;
		new->rchild = NULL;
		if (Data < pre->data)
			pre->lchild = new;
		else if (Data > pre->data)
			pre->rchild = new;
		return;
	}
	else
	{
		*b = 100;
		return;
	}
	
}
//初始化
void InitTree(BitNode* treeroot, BitNode* p, BitNode* f,int*l)
{
	int b;
	if (treeroot == NULL)
		return;
	treeroot->lchild = NULL;
	treeroot->rchild = NULL;
	int i;
	char node[1000]="";
	retry:printf("请输入你要放入的值(只能放0到10的不同的整数，连着输入别加空格，将就用着):");
	scanf("%s", node);
	int length = strlen(node);
	if ((node[0] - '0') % 1 != 0)
	{
		printf("输入错误");
		goto retry;
	}

	treeroot->data = node[0] - '0';
	for (i = 1; i < length; i++)
	{
		if ((node[i] - '0') % 1 != 0)
		{
			printf("输入错误");
			goto retry;
		}
		p = NULL;
		f = NULL;
		AddNode(treeroot, node[i] - '0',&b,p,f);
		if (b == 100)
		{
			*l = b;
			return;
		}
	}
}
//删除
void DeleteNode(BitNode* treeroot, int Data, BitNode* p1, BitNode* f)
{
	BitNode* q,*s;
	int i = 0;
	BitNode *p= DeleteSearch(treeroot, Data,&i,p1,&f);
	if (i == 1)
	{
		printf("没有此项\n");
		return;
	}
	else if (p == treeroot)
	{
		printf("树根不可删除\n");
		return;
	}
	else if (p->rchild == NULL)
	{
		q = p;
		if (p->data < f->data)
			f->lchild = p->lchild;
		else
			f->rchild = p->lchild;
		free(q);
	}
	else if (p->lchild == NULL)
	{
		q = p;
		if (p->data < f->data)
			f->lchild = p->rchild;
		else
			f->rchild = p->rchild;
		free(q);
	}
	else
	{
		q = p;
		s = p->lchild;
		while (s->rchild)
		{
			q = s;
			s = s->rchild;
		}
		p->data = s->data;
		if (q != p)
			q->rchild = s->lchild;
		else
			q->lchild = s->lchild;
		free(s);
	}
	printf("删除成功\n");
	return;
}


//前序遍历
void PreOrderTraverse(BitNode* T)
{
	if (T == NULL)
	{
		{
			printf("\n");
			return; 
		}
	}
	printf("%d ", T->data);
	PreOrderTraverse(T->lchild);
	PreOrderTraverse(T->rchild);
}
//中序遍历
void InOrderTraverse(BitNode* T)
{
	if (T == NULL)
	{
		printf("\n");
		return;
	}
	InOrderTraverse(T->lchild);
	printf("%d", T->data);
	InOrderTraverse(T->rchild);
}
//后序遍历
void PostOrderTraverse(BitNode* T)
{
	if (T == NULL)
	{
		printf("\n");
		return;
	}
	PostOrderTraverse(T->lchild);
	PostOrderTraverse(T->rchild);
	printf("%d", T->data);
}

// 层序遍历
void LevelOrderTraversal(BitNode* treeroot) 
{
	if (treeroot == NULL) 
	{
		return;
	}

	Queue queue; // 创建一个队列用于层序遍历
	initQueue(&queue);

	enqueue(&queue, treeroot); // 将根节点入队

	while (queue.front != NULL) 
	{
		BitNode* current = dequeue(&queue); // 出队并访问当前节点
		printf("%d ", current->data);

		// 如果有左子树，将左子树节点入队
		if (current->lchild != NULL) 
		{
			enqueue(&queue, current->lchild);
		}

		// 如果有右子树，将右子树节点入队
		if (current->rchild != NULL) 
		{
			enqueue(&queue, current->rchild);
		}
	}
	printf("\n");
}

int main()
{
	BitNode* treeroot=(BitNode*)malloc(sizeof(BitNode));
	int input;//初始化输入值
	int m,b,a,l;
	BitNode* p = NULL;
	BitNode* f = NULL;
	InitTree(treeroot,p,f,&l);
	if (l == 100)
	{
		printf("入树失败，有重复元素，请重新运行程序\n");
		return;
	}
	do
	{
		input = 10000000;
		menu();
		retry:printf("请选择你要进行的操作:");
		scanf("%d", &input);
		getchar();
		if (input==10000000)
		{
			printf("输入错误\n");
			goto retry;
		}
		switch (input)
		{
		case 1:
			a = 10000000;
		retry1:printf("添加\n请输入你要入树的元素：");
			scanf("%d", &a);
			getchar();
			if (a== 10000000)
			{
				printf("输入错误\n");
				goto retry1;
			}
			p = NULL;
			f = NULL;
			AddNode(treeroot, a,&b,p,f);
			if (b)
				printf("添加成功\n");
			else
				printf("添加失败\n");
			break;
		case 2:
			a = 10000000;
			retry2:printf("删除\n请输入你要删除的元素：");
			scanf("%d", &a);
			getchar();
			if (a == 10000000)
			{
				printf("输入错误\n");
				goto retry2;
			}
			p = NULL;
			f = NULL;
			DeleteNode(treeroot, a,p,f);
			break;
		case 3:
			a = 10000000;
			retry3:printf("查找\n请输入你要查找的元素：");
			scanf("%d", &a);
			getchar();
			if (a == 10000000)
			{
				printf("输入错误\n");
				goto retry3;
			}
			p = NULL;
			f = NULL;
			Search(treeroot, a, &b,p,f);
			if (b)
				printf("没有此项\n");
			else
				printf("查找成功，有此项\n");
			break;
		case 4:
			printf("1.前序 2.中序 3.后序\n");
			retry4:printf("请选择你要遍历的方式：");
			scanf("%d", &m);
			getchar();
			if (m!= 1&&m!=2&&m!=3)
			{
				printf("输入错误\n");
				goto retry4;
			}
			switch (m)
			{
			case 1:
				PreOrderTraverse(treeroot);
				break;
			case 2:
				InOrderTraverse(treeroot);
				break;
			case 3:
				PostOrderTraverse(treeroot);
				break;
			}
			break;
		case 5:
			LevelOrderTraversal(treeroot);
			break;
		case 0:
			printf("退出\n");
			break;
		}
	} while (input);
}
#include "队列.h"
// --------------------
//|     head           |<-rear(空时) 
//|--------------------|
//|                    |<-rear（当有一个元素时）
//|--------------------|
//|                    |     
//|--------------------|
//|                    |
//|--------------------|
//|                    |
// --------------------

//菜单
void menu()
{
	printf("********************************\n");
	printf("***           菜单           ***\n");
	printf("***1.入队              2.出队***\n");
	printf("***3.查找              4.打印***\n");
	printf("***5.销毁              0.退出***\n");
	printf("********************************\n");
}

//链式队列节点
typedef struct Linknode
{
	int data;
	struct Linknode* next;
}Linknode;

typedef struct LinkQueue
{
	Linknode* head, * rear;
}LinkQueue;

//链式队列初始化
void InitQueue(LinkQueue* Q)
{
	if (Q == NULL)
		return;
	Q->head = NULL;
	Q->rear = NULL;
}

//入队
void EnQueue(LinkQueue* Q, int a)
{
	Linknode* p = (Linknode*)malloc(sizeof(Linknode));///////////////写入失败，第二个问题
	p->data = a;
	p->next = NULL;
	if (Q->head == NULL) 
	{
		Q->head = p;
		Q->rear = p;
	}
	else 
	{
		Q->rear->next = p;
		Q->rear = p;
	}
	printf("入队成功\n");
}

//出队
void DeQueue(LinkQueue* Q)
{
	if (Q->head == NULL) 
	{
		printf("队列为空\n");
		return;
	}
	Linknode* p = Q->head;
	int a = p->data;
	Q->head = Q->head->next;
	free(p);
	printf("队头%d已出队\n",a);
	return;
}

//查找
int Search(LinkQueue* Q, int a,int*b)
{
	int i;
	if (Q->head == NULL)
	{
		printf("队列为空\n");
		return;
	}
	Linknode* p = Q->head;
	for (i = 1; p != NULL; i++)
	{
		if (p->data = a)
		{
			return i;
		}
	}
	*b = 0;
	printf("该队列没有这个元素");
	i = 0;
	return i;
}

//遍历后打印
void Print(LinkQueue* Q)
{
	if (Q->head == NULL)
	{
		printf("队列为空\n");
		return;
	}
	Linknode* p = Q->head;
	while (p!=NULL)
	{
		printf("%d ", p->data);
		p = p->next;
	}
	printf("\n");
	return;
}

//销毁队列
void DeleteQueue(LinkQueue* Q)
{
	int a;
	if (Q->head == NULL) {
		free(Q);
		printf("销毁成功");
		return;
	}
	Linknode* p = Q->head;
	int i,j;
	for (i=1;p != NULL;i++)
	{
		printf("%d ", p->data);
		p = p->next;
	}
	printf("确定销毁请输入任意数字，没有否则：");
	scanf("%d", &a);
	for (j = 0; j < i; j++)
	{
		DeQueue(Q);
	}
	free(Q);
	printf("销毁成功\n");
	return;
}

int main()
{
	LinkQueue* Q = (LinkQueue*)malloc(sizeof(LinkQueue));//???????????????????????????????????????第一问题
	InitQueue(Q);
	int input;//初始化输入值
	do
	{
		input = 100000;
		int a;
		menu();
		printf("请选择你要进行的操作:");
		scanf("%d", &input);
		getchar();
		if(input == 100000)
			printf("输入错误\n");
		switch (input)
		{
		case 1:
			a = 100000;
			retry:printf("入队\n请输入你要入队的元素(整数)：");
			scanf("%d", &a);
			getchar();
			if (a != 100000)
				EnQueue(Q, a);
			else
			{
				printf("输入错误\n");
				goto retry;
			}
			break;
		case 2:
			printf("出队\n");
			DeQueue(Q);
			break;
		case 3:
			a = 100000;
		retry1:printf("查找\n请输入你要查找的元素：");
			scanf("%d", &a);
			getchar();
			if (a != 100000)
			{
				a = Search(Q, a,&a);
				if (a != 0)
				{
					printf("这个元素在第%d个\n", a);
				}
				break;
			}
			else
			{
				printf("输入错误\n");
				goto retry1;
			}
			
		case 4:
			printf("打印\n");
			Print(Q);
			break;
		case 5:
			printf("销毁\n");
			DeleteQueue(Q);
			break;
		case 0:
			printf("退出\n");
			break;
		default:
			printf("输入错误\n");
		}
	} while (input);
	return 0;
}
#include <stdio.h>
#include <stdlib.h> 

typedef struct node 
{
	int data;
	struct node* left;
	struct node* right;
}Node,*N;

typedef struct tree
{
	Node* root;
}Tree;

typedef struct queue
{
	int element[100];
	int head;
	int tail;
}Queue;

Queue que;
//初始化树
void make_empty(Node *node);
//插入(左边的数据比结点小，右边比结点数据大） 
void insert(Tree* tree,int data);
//查找
void search(Node* node,int data); 
//获取树的高度
int height(Node* node);
//先序遍历 
void preorder(Node* node);
//中序遍历
void inorder(Node* node);
//后序遍历
void posorder(Node* node);

 //销毁二叉树 
void destroy(Node *node);
//入队
int queuein(Queue* p,int data);
//出队
int queueout(Queue *p,int *result);
//层序遍历
void leverorder(Node *node);

//删除 
Node* delete(Node *node,int del)
{
	if(node == NULL)
	{
		return NULL;
	}
	if(node->data <del)
	{
		node->right = delete(node->right ,del);
		return node;
	}
	else if(node->data > del)
	{
		node->left = delete(node->left ,del);
		return node;
	}
	else if(node->data ==del)
	{
		if(node->left == NULL && node->right == NULL)//左右无节点 
		{
			Node *temp = node;
			free(node);
			temp = NULL;
			return temp;
		}
		else if(node->left == NULL &&node->right != NULL)//只有右节点 
		{
			Node *temp = node->right ;
			free(node);
			return temp;
		}
		else if(node->left != NULL && node->right == NULL)
		{
			Node *temp = node->left ;
			free(node);
			return temp;
		}
		else if(node->left != NULL && node->right != NULL)
		{
			Node* cur = node->right ;
			while(cur->left != NULL)
			{
				cur = cur->left ;
			}
			cur->left = node->left  ;
			Node *temp = node->right ;
			free(node);
			return temp;
		}
		return node;
	}
}
int queuein(Queue* p,int data);

int main()
{
	Tree tree;
	tree.root  = NULL;
	make_empty(tree.root ); 
	
	
	int value[] = {5,3,7,8,9,4,6};
	printf("初始化输入的数据为5，3，7，8，9，4，6\n");
	int i = 0;
	for (i ; i<7; i++)
	{
		insert(&tree ,value[i]);
	 } 
	preorder(tree.root );
	printf("\n");
	inorder(tree.root );
	printf("\n");
	posorder(tree.root );
	printf("\n");
	
	int h = height(tree.root );
	printf("h = %d\n",h);
	
	printf("输入你需要查找的数据：\n");
	int ser = 0;
	scanf("%d",&ser);
	search(tree.root ,ser);
	
	printf("请输入你需要删除的数据：\n");
	int del = 0;
	scanf("%d",&del);
	//printf("1\n");
	Node *node;
	node = delete(tree.root ,del);
	//printf("2\n");
	preorder(node );
	printf("\n");
	inorder(node );
	printf("\n");
	posorder(node );
	printf("\n");
	if(node != NULL)
	{
		queuein(&que,node->data );
	    leverorder(node);
	}
	
	
	return 0;
}


//初始化树
void make_empty(Node *node)
{
	if(node != NULL)
	{
		make_empty(node->left );
		make_empty(node->right );
		free(node);
	}
	return ;
}
//查找
void search(Node *node,int data) 
{
	Node *p = node;
	if( p->data  == data)
	{
		printf("查找成功！\n");
	}
	else if(data < p->data  )
	{
		p = p->left ;
	}
	else if(data > node->data )
	{
		p = p->right ;
	}
    else
	{
		printf("抱歉，没有找到该数据\n");
	}
}
//插入(左边的数据比结点小，右边比结点数据大） 
void insert(Tree* tree,int data) 
{
	Node* node ;
	node = (Node*)malloc(sizeof(node));
	if(node == NULL)
	{
		printf("程序出错");
		return;
	}
	node->data = data;
	node->left = NULL;
	node->right = NULL;
	
	if(tree->root  == NULL)
	{
		tree->root = node;
	}
	else
	{
		Node* temp;
		temp = tree->root ;
		while (temp != NULL)
		{
			if(data < temp->data )
			{
				if(temp->left == NULL)
				{
					temp->left = node;
					return;
				}
				else
				{
					temp = temp->left ;
				}
			}
			else
			{
				if(temp->right == NULL)
				{
					temp->right = node;
					return;
				}
				else
				{
					temp = temp->right ;
				}
			}
		}
	}
}

//获取树的高度
int height(Node* node)
{
	if(node == NULL)
	{
		return 0;
	}
	else
	{
		int left_h = height(node->left );
		int right_h = height(node->right );
		int max;
		if(left_h < right_h)
		{
			max = right_h;
		}
		else
		{
			max = left_h;
		}
		return max + 1;
	}
 } 

//先序遍历 
void preorder(Node* node)
{
	if(node != NULL)
	{
		printf("%d",node->data );
		preorder(node->left );
		preorder(node->right );
	}
}

//中序遍历
void inorder(Node* node)
{
	if(node != NULL)
	{
		inorder(node->left );
		printf("%d",node->data );
		inorder(node->right );
	}
 } 

//后序遍历
void posorder(Node* node)
{
	if (node != NULL)
	{
		posorder(node->left );
		posorder(node->right );
		printf("%d",node->data );
	}
 } 
 //销毁二叉树 
void destroy(Node *node)
{
	if(node != NULL);
	{
		destroy(node->left );
		destroy(node->right );
		free(node);
	}
}
 
//入队
int queuein(Queue* p,int data)
{
	//队列满了
	if(p->tail >99)
	{
		return 0;
	}
	p->element[p->tail ] = data;
	p->tail++;
	return 1;
 } 
 
//出队
int queueout(Queue *p,int *result)
{
	if(p->head == p->tail )
	{
		return 0;
	}
	*result = p->element[p->head++];
	return 1;
 } 
 
//层序遍历
void leverorder(Node *node)
{
	if(node == NULL)
	{
		return;
	}
	
	//输出队列中保留的信息
	int result;
	while(queueout(&que,&result) == 1)
	{
		printf("%d",result);
	 } 
	//将子节点先加入队列，后续再遍历更深的层次
	if(node->left != NULL)
	{
		Node *temp = node->left ;
		queuein(&que,temp->data );
	 } 
	if(node->right != NULL)
	{
		Node *temp = node->right ;
		queuein(&que,temp->data  );
	}
	leverorder(node->left );
	leverorder(node->right );
 } 

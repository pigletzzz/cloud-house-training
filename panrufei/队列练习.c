#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h> 

//定义队列结点 
typedef struct Node
{
	int data;
	struct Node *next;
}QueueNode;

typedef struct Queue
{
	QueueNode *front,*rear;
}LinkQueue;
//初始化队列
void Init(LinkQueue *s)
{
	QueueNode *p;
	p=(QueueNode*)malloc(sizeof(QueueNode));
	p->next = NULL;
	s->front = s->rear = p;
}
//判断队列是否为空
int Empty(LinkQueue *s)
{
	if(s->front == s->rear )
	{
		return 1;
	}
	return 0;
 } 
//入队
void add (LinkQueue *s,int data)
{
	QueueNode *p;
	p=(QueueNode*)malloc(sizeof(QueueNode));
	if(p == NULL)
	{
		printf("动态分配内存失败\n");
		return;
	}
	p->data =data;
	p->next =NULL;
	s->rear ->next =p;//先接入链表 
	s->rear =p; //更新尾部结点 
}
//出队
void del(LinkQueue *s)
{
	if(s->front == s->rear )
	{
		printf("队列为空\n");
		return;
	}
	QueueNode *p;
	p = s->front->next ;
	if(p == s->rear )
	{
		s->front = s->rear ;
	}
	s->front->next = p->next ;
	free(p);
}
//查询
int search(LinkQueue *s)
{
	int i =1;
	int data;
	QueueNode *p;
	p = s->front->next ;
	printf("请输入你想搜索的数据：\n");
	scanf("%d",&data);
	while(p != NULL && p->data != data)
	{
		p = p->next ;
		i++;
	}
	if(p == NULL)
	{
		printf("队列中没有这个数据\n");
		return 0;
	}
	printf("这个数据为%d，在第%d个\n",p->data ,i);
}

//遍历 
void printlist(LinkQueue *s)
{
	QueueNode *p;
	p = s->front->next ;
	while (p != NULL)
	{
		printf("%d",p->data );
		p = p->next ;
	}
}

//释放内存函数
void deletelist(LinkQueue *s) 
{
	QueueNode *p;
	p = s->front->next ;
	while ( p != NULL)
	{
		QueueNode *temp = p;
		p = p->next;
		free(temp);
	}
	s->front = NULL;//释放完所有节点后，将节点指向NULL 
}

int main()
{
	int i,data;
	LinkQueue s;
	//对这个队列初始化 
	Init(&s);
	i = Empty(&s);
	printf("当前队列是否为空，为空返回真1 ，不为空返回假0：%d\n",i);
	//执行入队操作
	add(&s,3);
	add(&s,4);
	add(&s,5);
	printf("当前队列元素有：\n");
	printlist(&s);
	printf("\n");
	//执行出队操作
	printf("现在进行出队操作\n");
	del(&s);
	printf("当前队列元素有：\n");
	printlist(&s);
	printf("\n");
	//查询队列元素
	search(&s) ;
	return 0;
}
